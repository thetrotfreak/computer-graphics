#include <graphics.h>

void cirlce_midpoint(int r)
{

    int tx = getmaxx() / 2;
    int ty = getmaxy() / 2;

    int x = 0;
    int y = r;

    int param = 1 - r;

    while (x < y)
    {
        putpixel(x + tx, y + ty, 15);
        putpixel(y + tx, x + ty, 15);
        putpixel(y + tx, -x + ty, 15);
        putpixel(x + tx, -y + ty, 15);
        putpixel(-x + tx, -y + ty, 15);
        putpixel(-y + tx, -x + ty, 15);
        putpixel(-y + tx, x + ty, 15);
        putpixel(-x + tx, y + ty, 15);
        x++;
        y = param < 0 ? y : (y - 1);
        param = param < 0 ? (param + (2 * x) + 1) : (param + 2 * (x - y) + 1);
    }
}

int main()
{
    int gd = DETECT, gm;
    initgraph(&gd, &gm, "");
    cirlce_midpoint(55);
    getch();
    return 0;
}