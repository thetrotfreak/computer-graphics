#include <graphics.h>
#include <math.h>

int main()
{
    int gd = DETECT, gm;
    initgraph(&gd, &gm, "");

    int t[8] = {100, 100, 150, 200, 50, 200, 100, 100};
    drawpoly(4, t);

    double theta = 45.0 * 3.14 / 180;
    double s = sin(theta);
    double c = cos(theta);

    for (int i = 0; i < 7; i += 2)
    {
        // x rotation clockwise
        t[i] = floor(t[i] * c + t[i + 1] * s);
        // y totation clockwise
        t[i + 1] = floor(t[i + 1] * c - t[i] * s);
    }

    drawpoly(4, t);
    getch();
    return 0;
}
