#include <stdio.h>
#include <graphics.h>

int main()
{
    int gd = DETECT, gm;
    initgraph(&gd, &gm, "");

    int n;
    printf("Number of edges of polygon? ");
    scanf("%d", &n);

    if (n >= 3)
    {
        int len = (n + 1) * 2;
        int *ptr_poly_points = (int *)malloc(sizeof(int) * len);

        if (ptr_poly_points != NULL)
        {
            int count = 1;
            for (int i = 0; i < len; i += 2)
            {
                if (i == len - 2)
                {
                    *(ptr_poly_points + i) = ptr_poly_points[0];
                    *(ptr_poly_points + i + 1) = ptr_poly_points[1];
                    break;
                }
                else
                {
                    int x, y;

                    printf("Coordinate %d? ", count++);
                    scanf("%d %d", &x, &y);

                    *(ptr_poly_points + i) = x;
                    *(ptr_poly_points + i + 1) = y;
                }
            }
            drawpoly(n + 1, ptr_poly_points);
            getch();
        }
    }
    return 0;
}