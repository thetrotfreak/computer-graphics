#include <stdio.h>
#include <graphics.h>

void line_bresenham(int x1, int y1, int x2, int y2)
{
    int delta_x = x2 - x1;
    int delta_y = y2 - y1;
    int param = (2 * delta_y) - delta_x;
    int i = 0;
    while (x1 < x2 && i < delta_x)
    {
        putpixel(x1, y1, 14);
        x1++;
        y1 = param < 0 ? y1 : (y1 + 1);
        param = param < 0 ? (param + (2 * delta_y)) : (param + (2 * delta_y) - (2 * delta_x));
        i++;
    }
}

int main()
{
    int gd = DETECT, gm;
    initgraph(&gd, &gm, "");
    int x1, y1, x2, y2;
    printf("Line end-points(space separated)? ");
    scanf("%d %d %d %d", &x1, &y1, &x2, &y2);
    line_bresenham(x1, y1, x2, y2);
    getch();
    return 0;
}