#include <stdio.h>
#include <graphics.h>

int main()
{
    int gd = DETECT, gm;
    initgraph(&gd, &gm, "");
    int r;
    printf("Radius of circle? ");
    scanf("%d", &r);
    circle(getmaxx() / 2, getmaxy() / 2, r);
    getch();
    return 0;
}