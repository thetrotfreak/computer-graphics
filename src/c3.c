#include <stdio.h>
#include <graphics.h>

int main()
{
    int gd = DETECT, gm;
    initgraph(&gd, &gm, "");

    unsigned int thickness = 1;
    printf("Enter Pixel Thickness(odd) : ");
    scanf("%d", &thickness);
    thickness = (thickness % 2 != 0) ? thickness : thickness + 1;

    int x = 100;
    int y = 100;
    int c = 14;

    int pixel = 1;
    int m = thickness / 2;
    for (int i = thickness - 1; i >= 0; i--)
    {
        for (int j = thickness - 1; j >= 0; j--)
        {
            printf("Pixel %d : (%d, %d)\n", pixel, (x - (j - m)), (y - (i - m)));
            putpixel((x - (j - m)), (x - (i - m)), c);
            pixel++;
        }
    }

    getch();
    return 0;
}