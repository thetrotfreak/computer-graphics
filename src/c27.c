#include <graphics.h>
#include <math.h>

int main()
{
    int gd = DETECT, gm;
    initgraph(&gd, &gm, "");

    int t[8] = {100, 100, 150, 200, 50, 200, 100, 100};
    drawpoly(4, t);

    int ttemp[8];
    memcpy(ttemp, t, sizeof(t));

    // translation
    int tx = 110, ty = 0;
    for (int i = 0; i < 7; i += 2)
    {
        ttemp[i] += tx;
        ttemp[i + 1] += ty;
    }
    drawpoly(4, ttemp);

    // rotation
    memcpy(ttemp, t, sizeof(t));
    double theta = 20 * 3.14 / 180;
    double s = sin(theta);
    double c = cos(theta);

    for (int i = 0; i < 7; i += 2)
    {
        // x rotation clockwise
        ttemp[i] = floor(ttemp[i] * c + ttemp[i + 1] * s);
        // y totation clockwise
        ttemp[i + 1] = floor(ttemp[i + 1] * c - ttemp[i] * s);
    }
    drawpoly(4, ttemp);

    // scaling
    memcpy(ttemp, t, sizeof(t));
    int sx = 2, sy = 2;
    for (int i = 0; i < 7; i += 2)
    {
        ttemp[i] *= sx;
        ttemp[i + 1] *= sy;
    }
    drawpoly(4, ttemp);
    getch();
    return 0;
}