#include <graphics.h>

int main()
{
    int gd = DETECT, gm;
    initgraph(&gd, &gm, "");
    int t[8] = {100, 100, 150, 200, 50, 200, 100, 100};
    drawpoly(4, t);
    // reflection x=y
    for (int i = 0; i < 7; i += 2)
    {
        int temp = t[i];
        t[i] = t[i + 1];
        t[i + 1] = temp;
    }
    drawpoly(4, t);
    getch();
    return 0;
}