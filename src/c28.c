#include <graphics.h>

int main()
{
    int gd = DETECT, gm;
    initgraph(&gd, &gm, "");
    line(100, 100, 150, 150);
    int sx = 2;
    line(100 + sx * 100, 100, 150 + sx * 150, 150);
    int tx = 100;
    line(100 + tx, 100, 150 + tx, 150);
    getch();
    return 0;
}