#include <graphics.h>

int main()
{
    int gd = DETECT, gm;
    initgraph(&gd, &gm, "");
    int triangle[] = {
        150, 150,
        100, 250,
        200, 250,
        150, 150
    };
    drawpoly(4, triangle);
    getch();
    return 0;
}