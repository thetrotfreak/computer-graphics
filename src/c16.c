#include <graphics.h>
#include <stdio.h>

#define color 14

void cirlce_midpoint(int r)
{
    // translate to window center
    int tx = getmaxx() / 2;
    int ty = getmaxy() / 2;

    // initital coordinates
    int x = 0;
    int y = r;

    // initial decision parameter
    int param = 1 - r;

    while (x < y)
    {
        /*
        coordinate test
        printf("%d %d %d\n", param, x, y);
        */
        // octant 1
        putpixel(x + tx, y + ty, color);
        // octant 2
        putpixel(y + tx, x + ty, color);
        // octant 3
        putpixel(y + tx, -x + ty, color);
        // octant 4
        putpixel(x + tx, -y + ty, color);
        // octant 5
        putpixel(-x + tx, -y + ty, color);
        // octant 6
        putpixel(-y + tx, -x + ty, color);
        // octant 7
        putpixel(-y + tx, x + ty, color);
        // octant 8
        putpixel(-x + tx, y + ty, color);
        x++;
        y = param < 0 ? y : (y - 1);
        param = param < 0 ? (param + (2 * x) + 1) : (param + 2 * (x - y) + 1);
    }
    /*
    coordinate test
    printf("%d %d %d\n", param, x, y);
    */
}

int main()
{
    int gd = DETECT, gm;
    initgraph(&gd, &gm, "");
    int n, r;
    printf("Number of concentric circles? ");
    scanf("%d", &n);
    for (int i = 0; i < n; i++)
    {
        printf("Radius %d? ", i + 1);
        scanf("%d", &r);
        cirlce_midpoint(r);
    }
    getch();
    return 0;
}