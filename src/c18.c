#include <graphics.h>
#include <stdio.h>

int main()
{
    int gd = DETECT, gm;
    initgraph(&gd, &gm, "");
    int sx, sy;
    printf("Scaling value? ");
    scanf("%d %d", &sx, &sy);
    line(100, 100, 150, 150);
    line(100 * sx, 100 * sy, 150 * sx, 150 * sy);
    getch();
    return 0;
}