#include <graphics.h>
#include <stdio.h>

int main()
{
    int gd = DETECT, gm;
    initgraph(&gd, &gm, "");
    int t[8] = {100, 100, 150, 200, 50, 200, 100, 100};
    drawpoly(4, t);
    int sx, sy;
    printf("Scaling value? ");
    scanf("%d %d", &sx, &sy);
    for (int i = 0; i < 7; i += 2)
    {
        t[i] *= sx;
        t[i + 1] *= sy;
    }
    drawpoly(4, t);
    getch();
    return 0;
}