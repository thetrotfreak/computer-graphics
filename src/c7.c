#include <graphics.h>

int main()
{
    int gd = DETECT, gm;
    initgraph(&gd, &gm, "");

    int square[] = {
        10, 10,
        100, 10,
        100, 100,
        10, 100,
        10, 10
    };

    int n = (sizeof(square) / sizeof(int)) / 2;
    drawpoly(n, square);
    getch();
    return 0;
}