#include <graphics.h>
#include <stdio.h>

int main()
{
    int x2;
    int y2;
    printf("Enter end-coordiantes of line : ");
    scanf("%d %d", &x2, &y2);

    int gd = DETECT, gm;
    initgraph(&gd, &gm, "");

    line(0, 0, x2, y2);

    getch();
    return 0;
}