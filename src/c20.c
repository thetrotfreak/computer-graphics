#include <graphics.h>
#include <math.h>

#define PI 3.14

int main()
{
    int gd = DETECT, gm;
    initgraph(&gd, &gm, "");
    int x0 = 100;
    int y0 = 100;
    int x1 = 100;
    int y1 = 200;
    line(x0, y0, x1, y1);
    double theta = 45.0;
    double c = cos(theta * PI / 180);
    double s = sin(theta * PI / 180);
    double x0d = floor(x0 * c + y0 * s);
    double y0d = floor(-x0 * s + y0 * c);
    double x1d = floor(x1 * c + y1 * s);
    double y1d = floor(-x1 * s + y1 * c);
    line(x0d, y0d, x1d, y1d);
    getch();
    return 0;
}