#include <graphics.h>

int main()
{
    int gd = DETECT, gm;
    initgraph(&gd, &gm, "");
    setfillstyle(SOLID_FILL, getmaxcolor());
    bar(0, 50, 150, 70);
    bar(0, 80, 170, 100);
    bar(0, 110, 130, 130);
    getch();
    return 0;
}