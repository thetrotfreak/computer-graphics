#include <graphics.h>
#include <stdio.h>
#include <math.h>

void line_dda(int x1, int y1, int x2, int y2)
{
    int delta_x = x2 - x1;
    int delta_y = y2 - y1;

    int steps = abs(delta_x) > abs(delta_y) ? abs(delta_x) : abs(delta_y);

    float x_inc = delta_x / steps;
    float y_inc = delta_y / steps;

    for (int i = 0; i <= steps; i++)
    {
        putpixel(round(x1), round(y1), 15);
        x1 += x_inc;
        y1 += y_inc;
    }
}

int main()
{
    int gd = DETECT, gm;
    initgraph(&gd, &gm, "");
    int x1, y1, x2, y2;
    printf("Line end-points(space separated)? ");
    scanf("%d %d %d %d", &x1, &y1, &x2, &y2);
    line_dda(x1, y1, x2, y2);
    getch();
    return 0;
}