#include <stdio.h>
#include <graphics.h>

int main()
{
    int gd = DETECT, gm;
    initgraph(&gd, &gm, "");
    for (int i = 0; i < 15; i++)
    {
        setbkcolor(i);
        printf("Set Background color to : %d\n", getbkcolor());
        delay(2000);
        cleardevice();
    }
    getch();
    return 0;
}