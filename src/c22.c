#include <graphics.h>
#include <stdio.h>

int main()
{
    int gd = DETECT, gm;
    initgraph(&gd, &gm, "");
    circle(getmaxx() / 2, getmaxy() / 2, 50);
    int tx, ty;
    printf("Translation value? ");
    scanf("%d %d", &tx, &ty);
    circle(getmaxx() / 2 + tx, getmaxy() / 2 + ty, 50);
    getch();
    return 0;
}