#include <graphics.h>

int main()
{
    int gd = DETECT, gm;
    initgraph(&gd, &gm, "");
    setfillstyle(SOLID_FILL, getmaxcolor());
    pieslice(getmaxx() / 2, getmaxy() / 2, 0, 45, 100);

    setfillstyle(SOLID_FILL, getmaxcolor() - 1);
    pieslice(getmaxx() / 2, getmaxy() / 2, 45, 75, 100);

    setfillstyle(SOLID_FILL, getmaxcolor() - 2);
    pieslice(getmaxx() / 2, getmaxy() / 2, 75, 165, 100);

    setfillstyle(EMPTY_FILL, getmaxcolor());
    pieslice(getmaxx() / 2, getmaxy() / 2, 165, 360, 100);
    getch();
    return 0;
}