> # Borland Graphics Interface
>
> Refer [WinBGIm](https://winbgim.codecutter.org/V6_0/doc/index.html) for available functions and their uses.

---

> # Mingw-64
>
> Download [here](https://sourceforge.net/projects/mingw-w64/).

---

> # Requires
> - graphics.h
>
> - libbgi.a

---

> # Compiler Flags
> Pass the following flags while compiling a WinBGIm.c file
>
> - -lbgi
>
> - -lgdi32
>
> - -lcomdlg32
>
> - -luuid
>
> - -loleaut32
>
> - -lole32
